package tati.curso.udemy.javacompleto.secao13.exercicio01.entities;

public class Department {
	
	private String name;

	public Department(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
