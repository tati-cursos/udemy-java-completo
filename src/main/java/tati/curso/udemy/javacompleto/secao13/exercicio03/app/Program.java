package tati.curso.udemy.javacompleto.secao13.exercicio03.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.Client;
import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.Order;
import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.OrderItem;
import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.Product;
import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.enums.OrderStatus;

public class Program {

	public static void main(String[] args) throws ParseException {
		
		Scanner sc = new Scanner(System.in);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		System.out.println("Enter client data:");
		System.out.print("Name: ");
		String clientName = sc.nextLine();
		System.out.print("Email: ");
		String email = sc.nextLine();
		System.out.print("Birth Date (DD/MM/YYYY): ");
		Date birthDate = sdf.parse(sc.nextLine());
		System.out.println("Enter order data:");
		System.out.print("Status: ");
		String status = sc.nextLine();
		System.out.print("How many items to this order? ");
		Integer quantityOfItems = sc.nextInt();
		sc.nextLine();
		
		Order order = 
				new Order(
				new Date(), OrderStatus.valueOf(status), 
				new Client(clientName, email, birthDate));
		
		getItemsData(quantityOfItems, sc, order);
		
		System.out.println();
		System.out.println("ORDER SUMMARY:");
		System.out.println(order.getOrderSumary());
		
				
		sc.close();
	}
	
	private static void getItemsData(Integer quantityOfItems, Scanner sc, Order order) {
		for (int i = 1; i <= quantityOfItems; i++) {
			System.out.println("Enter #" + i + " item data:");
			System.out.print("Product name: ");
			String productName = sc.nextLine();
			System.out.print("Product price: ");
			Double productPrice = sc.nextDouble();
			System.out.print("Quantity: ");
			Integer quantityOfProduct = sc.nextInt();
			sc.nextLine();
			OrderItem orderItem =
					new OrderItem(quantityOfProduct, productPrice,
					new Product(productName, productPrice));
			
			order.addItem(orderItem);
		}
	}

}
