package tati.curso.udemy.javacompleto.secao13.exercicio03.entities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tati.curso.udemy.javacompleto.secao13.exercicio03.entities.enums.OrderStatus;

public class Order {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	private Date moment;
	private OrderStatus status;
	private Client client;
	
	private List<OrderItem> items = new ArrayList<>();
	
	public Order(Date moment, OrderStatus status, Client client) {
		this.moment = moment;
		this.status = status;
		this.client = client;
	}
	
	public Date getMoment() {
		return moment;
	}
	
	public OrderStatus getStatus() {
		return status;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void addItem(OrderItem item) {
		items.add(item);
	}
	
	public void removeItem(OrderItem item) {
		items.remove(item);
	}
	
	public Double total() {
		Double result = 0.0;
		for (OrderItem i : items) {
			result += i.subTotal();
		}
		return result;
	}
	
	public String getOrderSumary() {
		StringBuilder sb = new StringBuilder();
		sb.append("Order moment: ");
		sb.append(sdf.format(moment) + "\n");
		sb.append("Order status: ");
		sb.append(status + "\n");
		sb.append("Client: ");
		sb.append(client.getClientInfo() + "\n");
		sb.append("Order items:" + "\n");
		for (OrderItem i : items) {
			sb.append(i.getOrderItems() + "\n");
		}
		sb.append("Total price: $");
		sb.append(total());
		return sb.toString();
	}
}
