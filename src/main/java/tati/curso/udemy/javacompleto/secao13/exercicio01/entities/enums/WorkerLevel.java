package tati.curso.udemy.javacompleto.secao13.exercicio01.entities.enums;

public enum WorkerLevel {
	JUNIOR,
	MID_LEVEL,
	SENIOR;
}
