package tati.curso.udemy.javacompleto.secao14.exercicio01.entities;

public class Employee {
	
	protected String name;
	protected Integer hours;
	protected Double valuePerHour;
	
	public Employee(String name, Integer hours, Double valuePerHour) {
		this.name = name;
		this.hours = hours;
		this.valuePerHour = valuePerHour;
	}

	public String getName() {
		return name;
	}

	public Integer getHours() {
		return hours;
	}

	public Double getValuePerHour() {
		return valuePerHour;
	}
	
	public Double payment() {
		return valuePerHour * hours;
	}
	
	public String getInfo() {
		return name + " - $ " + payment();
	}
	
}
