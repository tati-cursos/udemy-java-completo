package tati.curso.udemy.javacompleto.secao14.exercicio03.entities;

public class Company extends TaxPayer {
	
	private Integer numberOfEmployees;

	public Company(String name, Double annualIncome, Integer numberOfEmployees) {
		super(name, annualIncome);
		this.numberOfEmployees = numberOfEmployees;
	}

	public Integer getNumberOfEmployees() {
		return numberOfEmployees;
	}

	@Override
	public Double taxCalculation() {
		if (numberOfEmployees > 10) {
			return getAnnualIncome() * 0.14;
		}
		else {
			return getAnnualIncome() * 0.16;
		}
	}

	@Override
	public String taxesPaid() {
		return getName()
				+ ": $ "
				+ String.format("%.2f", taxCalculation());
	}
}
