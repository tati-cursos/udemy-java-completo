package tati.curso.udemy.javacompleto.secao14.exercicio03.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import tati.curso.udemy.javacompleto.secao14.exercicio03.entities.Company;
import tati.curso.udemy.javacompleto.secao14.exercicio03.entities.Individual;
import tati.curso.udemy.javacompleto.secao14.exercicio03.entities.TaxPayer;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<TaxPayer> payers = new ArrayList<>();
		
		System.out.print("Enter the number of tax payers: ");
		int numberOfPayers = sc.nextInt();
		
		for (int i=1; i<=numberOfPayers; i++) {
			System.out.println("Tax payer #" + i + " data:");
			System.out.print("Individual or company (i/c)? ");
			char payerType = sc.next().charAt(0);
			sc.nextLine();
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Anual income: ");
			Double annualIncome = sc.nextDouble();
			
			if (payerType == 'i') {
				System.out.print("Health expenditures: ");
				Double healthSpending = sc.nextDouble();
				payers.add(new Individual(name, annualIncome, healthSpending));
			}
			else {
				System.out.print("Number of employees: ");
				Integer numberOfEmployees = sc.nextInt();
				payers.add(new Company(name, annualIncome, numberOfEmployees));
			}
		}
		System.out.println();
		System.out.println("TAXES PAID:");
		for (TaxPayer p : payers) {
			System.out.println(p.taxesPaid());
		}
		
		Double result = 0.0;
		for (TaxPayer p : payers) {
			result += p.taxCalculation();
		}
		System.out.println();
		System.out.println("TOTAL TAXES: $ " + result);
		
		sc.close();
	}

}
