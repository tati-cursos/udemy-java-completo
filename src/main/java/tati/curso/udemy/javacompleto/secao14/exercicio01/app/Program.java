package tati.curso.udemy.javacompleto.secao14.exercicio01.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import tati.curso.udemy.javacompleto.secao14.exercicio01.entities.Employee;
import tati.curso.udemy.javacompleto.secao14.exercicio01.entities.OutsourcedEmployee;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Employee> employees = new ArrayList<>();
		
		System.out.print("Enter the number of employees: ");
		int numberOfEmployees = sc.nextInt();
		
		for(int i = 1; i <= numberOfEmployees; i++) {
			System.out.println("Employee #" + i + " data:");
			System.out.print("Outsourced (y/n)? ");
			char outsourced = sc.next().charAt(0);
			sc.nextLine();
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Hours: ");
			Integer hours = sc.nextInt();
			System.out.print("Value per hour: ");
			Double valuePerHour = sc.nextDouble();
			
			if(outsourced == 'y') {
				System.out.print("Additional charge: ");
				Double additionalCharge = sc.nextDouble();
				employees.add(new OutsourcedEmployee(name, hours, valuePerHour, additionalCharge));
			}
			else {
				employees.add(new Employee(name, hours, valuePerHour));				
			}
		}
		
		System.out.println();
		System.out.println("PAYMENTS:");
		for(Employee e : employees) {
			System.out.println(e.getInfo());
		}
		
		sc.close();
	}

}
