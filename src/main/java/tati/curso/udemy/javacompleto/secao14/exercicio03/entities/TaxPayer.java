package tati.curso.udemy.javacompleto.secao14.exercicio03.entities;

public abstract class TaxPayer {
	
	private String name;
	private Double annualIncome;
	
	public TaxPayer(String name, Double annualIncome) {
		this.name = name;
		this.annualIncome = annualIncome;
	}

	public String getName() {
		return name;
	}

	public Double getAnnualIncome() {
		return annualIncome;
	}
	
	public abstract Double taxCalculation();
	
	public abstract String taxesPaid();
	
}
