package tati.curso.udemy.javacompleto.secao14.exercicio01.entities;

public class OutsourcedEmployee extends Employee{
	
	private static double PERCENT = 1.1;

	private Double additionalCharge;

	public OutsourcedEmployee(String name, Integer hours, Double valuePerHour, Double additionalCharge) {
		super(name, hours, valuePerHour);
		this.additionalCharge = additionalCharge;
	}

	public Double getAdditionalCharge() {
		return additionalCharge;
	}
	
	@Override
	public Double payment() {
		return super.payment() + additionalCharge * PERCENT;
	}
	
	@Override
	public String getInfo() {
		return name + " - $ " + payment();
	}
}
