package tati.curso.udemy.javacompleto.secao14.exercicio03.entities;

public class Individual extends TaxPayer {
	
	private Double healthSpending;

	public Individual(String name, Double annualIncome, Double healthSpendin) {
		super(name, annualIncome);
		this.healthSpending = healthSpendin;
	}

	public Double getHealthSpendin() {
		return healthSpending;
	}

	@Override
	public Double taxCalculation() {
		if(getAnnualIncome() < 20000.0) {
			return (getAnnualIncome() * 0.15) - (healthSpending * 0.50);
		}
		else {
			return (getAnnualIncome() * 0.25) - (healthSpending * 0.50);
		}
	}

	@Override
	public String taxesPaid() {
		return getName()
				+ ": $ "
				+ String.format("%.2f", taxCalculation());
	}
}
