package tati.curso.udemy.javacompleto.secao14.exercicio02.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import tati.curso.udemy.javacompleto.secao14.exercicio02.entities.ImportedProduct;
import tati.curso.udemy.javacompleto.secao14.exercicio02.entities.Product;
import tati.curso.udemy.javacompleto.secao14.exercicio02.entities.UsedProduct;

public class Program {

	public static void main(String[] args) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Product> products = new ArrayList<>();
		
		System.out.print("Enter the number of products: ");
		int numberOfProduct = sc.nextInt();
		sc.nextLine();
		
		for (int i = 1; i <= numberOfProduct; i++) {
			System.out.println("Product #" + i + " data:");
			System.out.print("Common, used or imported (c/u/i)? ");
			char productType = sc.next().charAt(0);
			sc.nextLine();
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Price: ");
			Double price = sc.nextDouble();
			sc.nextLine();
			
			if (productType == 'i') {
				System.out.print("Customs fee: ");
				Double customsFee = sc.nextDouble();
				products.add(new ImportedProduct(name, price, customsFee));
				System.out.println();
			}
			else if (productType == 'u') {
				System.out.print("Manufacture date (DD/MM/YYYY): ");
				Date manufactureDate = sdf.parse(sc.nextLine());
				products.add(new UsedProduct(name, price, manufactureDate));
				System.out.println();
			}
			else {
				products.add(new Product(name, price));
				System.out.println();
			}
		}
		
		System.out.println("PRICE TAGS:");
		for (Product p : products) {
			System.out.println(p.priceTag());
		}
		sc.close();
	}

}
